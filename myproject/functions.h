#ifndef _project_0_h
#define _project_0_h

#include "aiaConfig.h"
#include "ucasConfig.h"
#include <list>
#include <opencv2/core/core.hpp>
#include <thread>

namespace selfDCar
{
	static cv::vector<float> timeE;
    //static cv::VideoWriter video("out3.avi",CV_FOURCC('M','J','P','G'),30,cv::Size(1280,720));
    //PARAMETERS
    const unsigned int HthreshLow  = 20;
	const unsigned int HthreshHigh = 25;
	const unsigned int Vthresh	   = 210;
    const size_t	   preFrame	   = 5;

	/*********Curve estimation - Data Structures***********/
	static std::vector<int> xLeftValues;
	static std::vector<int> yLeftValues;
	static std::vector<int> xRightValues;
	static std::vector<int> yRightValues;

	static std::vector<int> xUpLeftValues;
	static std::vector<int> yUpLeftValues;
	static std::vector<int> xUpRightValues;
	static std::vector<int> yUpRightValues;

	static cv::Mat leftCurveParameters    = cv::Mat(3,1,CV_32F,cv::Scalar(0.0));
	static cv::Mat rightCurveParameters   = cv::Mat(3,1,CV_32F,cv::Scalar(0.0));

	static cv::Mat leftUpCurveParameters  = cv::Mat(3,1,CV_32F,cv::Scalar(0.0));
	static cv::Mat rightUpCurveParameters = cv::Mat(3,1,CV_32F,cv::Scalar(0.0));


	static std::list<cv::Mat> leftPreviousParam;
	static std::list<cv::Mat> rightPreviousParam;

	static std::list<cv::Mat> leftUpPreviousParam;
	static std::list<cv::Mat> rightUpPreviousParam;

	static std::vector<cv::Point> leftPoints;
	static std::vector<cv::Point> rightPoints;

	static std::vector<cv::Point> leftUpPoints;
	static std::vector<cv::Point> rightUpPoints;

	//Variables
	static cv::Point offset, upperOffset;
	static cv::Mat	 curveFrame, processedFrame, upperProcessedFrame;
	/***********************************/
	//functions
    cv::Mat laneDetection		 (const cv::Mat & frame );
	void	findLines			 (cv::Mat & roi );
	void	cutHSV				 (const cv::Mat HSVchannels[],int HthreshLow, int HthreshHigh, int Vthresh,cv::Mat & output);
	void	curveEstimationLeft  ();
	void	buildLeftCurve		 (cv::Mat & originalImg );
	void	curveEstimationRight ();
	void	buildRightCurve		 (cv::Mat & originalImg );
	cv::Mat averageMatList		 (const std::list<cv::Mat> & list );

	void upperEstimation		   (const cv::Mat * input );
	void curveEstimationRightUpper ();
	void curveEstimationLeftUpper  ();
	void buildUpLeftCurve		   (cv::Mat & originalImg );
	void buildUpRightCurve		   (cv::Mat & originalImg );
	float getAvgTime ();

	//exception handling
	inline int myErrorHandler(int status, const char* func_name, const char* err_msg, const char* file_name, int line, void*){return 0;}

}

namespace utility
{

    class Line
	{
	private:
		float m;
		float q;
	public:
			 Line	   (const cv::Point p1,const cv::Point p2);
		int	 getXcoord (const int & yCoord);
	};

}

#endif // _project_0_h