// include aia and ucas utility functions
#include "aiaConfig.h"
#include "ucasConfig.h"

// include my project functions
#include "functions.h"



int main() 
{

	try
    {
		cvRedirectError(selfDCar::myErrorHandler);
	    aia::processVideoStream(std::string(EXAMPLE_IMAGES_PATH)+"/video3-curve.mp4", selfDCar::laneDetection);
		printf("\n\ntempo medio:%f\n",selfDCar::getAvgTime());
	}
	catch (aia::error &ex)
	{
		std::cout << "EXCEPTION thrown by " << ex.getSource() << "source :\n\t|=> " << ex.what() << std::endl;
	}
	catch (ucas::Error &ex)
	{
		std::cout << "EXCEPTION thrown by unknown source :\n\t|=> " << ex.what() << std::endl;
	}
}

