#include <iostream>
#include "functions.h"


/**

calcolo del tempo medio di elaborazione per frame

*/
float selfDCar::getAvgTime () {

	float appo = 0;

	for (auto &x : selfDCar::timeE) {

		appo += x;

	}
	return appo/selfDCar::timeE.size();
}

/**
funzione principale per la laneDetection
@cons cv::Mat &frame; frame in ingresso

*/
cv::Mat selfDCar::laneDetection (const cv::Mat & frame )
{
     ucas::Timer T;

	 //Timer per calcolo del tempo di elaborazione del frame
     T.start();

	 selfDCar::leftPoints.clear();
	 selfDCar::rightPoints.clear();

	 selfDCar::leftUpPoints.clear();
	 selfDCar::rightUpPoints.clear();

	
     cv::Mat *channels = new cv::Mat[3];
	 cv::Mat *upperChannels = new cv::Mat[3];

	 //output frame
	 curveFrame = frame.clone();

     //selezione ROI e conversione in HSV
	 cv::cvtColor(frame(cv::Rect(1*frame.cols/8,(11*frame.rows/16),6*frame.cols/8,5*frame.rows/16)),selfDCar::processedFrame,CV_BGR2HSV);
	 cv::cvtColor(frame(cv::Rect(6*frame.cols/16,(10*frame.rows/16),4*frame.cols/16,3*frame.rows/16)),selfDCar::upperProcessedFrame,CV_BGR2HSV);
	 
	 cv::split(selfDCar::processedFrame,channels); 
	 cv::split(selfDCar::upperProcessedFrame,upperChannels);

	 //Offset per recupero coordinate frame di partenza
	 selfDCar::offset = cv::Point (frame.cols/8,11*frame.rows/16);
	 selfDCar::upperOffset = cv::Point (6*frame.cols/16,(10*frame.rows/16));

	 //thread per stima della parte alta della corsia
	 std::thread upperCurvesThread (selfDCar::upperEstimation, upperChannels);

	 //operazioni nello spazio colore HSV (parte bassa della corsia)
	 selfDCar::cutHSV (channels,20,25,210,processedFrame);
	 //cv::imshow("processed frame",processedFrame);
     

	 /*MultiThread*/
	 
	 std::thread leftThread ( selfDCar::curveEstimationLeft );
	 std::thread rightThread ( selfDCar::curveEstimationRight );
	 
	 /*************/
	 //costruzione poligono da disegnare
	 std::vector<std::vector<cv::Point>> allPoints;
	 std::vector<cv::Point> aPoint;

     allPoints.push_back(aPoint);
	
	 //attesa terminazione dei diversi threads
	 leftThread.join();
	 rightThread.join();
	 upperCurvesThread.join();

	// cv::imshow("upperOut",selfDCar::upperProcessedFrame);

	//verifica della validitÓ (tramite circolaritÓ) della ROI superiore
	 std::vector<cv::Point> pointCheck;

	 pointCheck.reserve(selfDCar::leftUpPoints.size() + selfDCar::rightUpPoints.size());
	 pointCheck.insert(pointCheck.end(), selfDCar::leftUpPoints.begin(), selfDCar::leftUpPoints.end());
	 pointCheck.insert(pointCheck.end(), selfDCar::rightUpPoints.begin(), selfDCar::rightUpPoints.end());

	 double A = cv::contourArea(pointCheck);
	 double p = cv::arcLength(pointCheck, true);
	 double C = 4*ucas::PI*A / (p*p);				// circolaritÓ
	 //printf("circolaritÓ sup:%f\n",C);

	  if( C >= 0.318 && C <= 0.396) 
	  {  
		 allPoints[0].reserve( selfDCar::leftPoints.size() + selfDCar::rightPoints.size() + selfDCar::leftUpPoints.size() + selfDCar::rightUpPoints.size()); // preallocate memory
	 
		 allPoints[0].insert( allPoints[0].end(), selfDCar::leftPoints.begin(), selfDCar::leftPoints.end() );
		 allPoints[0].insert( allPoints[0].end(),  selfDCar::leftUpPoints.begin(),  selfDCar::leftUpPoints.end() );
		 allPoints[0].insert( allPoints[0].end(),  selfDCar::rightUpPoints.begin(),  selfDCar::rightUpPoints.end() );
		 allPoints[0].insert( allPoints[0].end(), selfDCar::rightPoints.begin(), selfDCar::rightPoints.end() );
	 
	  }
	  else 
	  { //viene scartata la stima sulla ROI superiore 
		  allPoints[0].reserve( selfDCar::leftPoints.size() + selfDCar::rightPoints.size());
		  allPoints[0].insert( allPoints[0].end(), selfDCar::leftPoints.begin(), selfDCar::leftPoints.end() );
		  allPoints[0].insert( allPoints[0].end(), selfDCar::rightPoints.begin(), selfDCar::rightPoints.end() );
		 // cv::waitKey();
	  }
			
	 


	 if ( allPoints[0].size() != 0)
	 { 
	    cv::fillPoly(curveFrame, allPoints,cv::Scalar(0,255,0));
		cv::addWeighted(curveFrame,0.5,frame,0.5,0,curveFrame); //trasparenza
	 }
	 
	 delete [] channels;
	 delete [] upperChannels;
	//printf("TIME: %f\n",T.elapsed<float>());
	 timeE.push_back(T.elapsed<float>());


	 
	// selfDCar::video.write(curveFrame);
	 
	 return curveFrame;
}

/**

 isola le infomazioni che caratterizzano i punti della segnaletica della corsia
@const cv::Mat HSVchannels[]; canali HSV del frame di partenza su cui operare
@int		   HthreshLow;	  soglia inferiore per tonalitÓ del frame (dedicato per ricerca di segnaletica gialla)
@int		   HtreshHigh;	  soglia superiore per tonalitÓ del frame (dedicato per ricerca di segnaletica gialla)
@int		   Vthresh;		  soglia per luminositÓ del frame (dedicato per la ricerca di segnaletica bianca)
@cv::Mat	   &output;		  matrice in uscita

*/
void selfDCar::cutHSV (const cv::Mat HSVchannels[],int HthreshLow, int HthreshHigh, int Vthresh,cv::Mat & output)
{
	
    selfDCar::xLeftValues.clear();
    selfDCar::yLeftValues.clear();
	selfDCar::xRightValues.clear();
    selfDCar::yRightValues.clear();
    output = cv::Mat (HSVchannels[0].rows,HSVchannels[0].cols,CV_8U,cv::Scalar(0)); 

	//limiti finestra di lavoro
	int rowLine = 14*output.rows/16;
	int colLine = 3*output.cols/9;

	utility::Line lSx = utility::Line ( cv::Point(colLine,0),cv::Point(0,rowLine) );
	utility::Line lDx = utility::Line ( cv::Point(output.cols-colLine+output.cols/9,0),cv::Point(output.cols,rowLine) );


	for ( int y = 0; y < 30*output.rows/32; y ++ )
	{
	    
	    const unsigned char * HtThRoW = HSVchannels[0].ptr<unsigned char>(y);
		const unsigned char * StThRoW = HSVchannels[1].ptr<unsigned char>(y);
		const unsigned char * VtThRoW = HSVchannels[2].ptr<unsigned char>(y);
		unsigned char * outThRoW = output.ptr(y);

		
		if ( y >= rowLine )
		{
		    
		    for ( int x = 0; x < output.cols; x++)
			{
			
			if ( x >= (output.cols/2 - output.cols/8) && x <= (output.cols/2 + output.cols/8) )
			{
			    continue;
			}
			
			if ((HtThRoW[x] >= HthreshLow && HtThRoW[x] <= HthreshHigh && (StThRoW[x] >= 100)) || VtThRoW[x] >= Vthresh )
			{
			    outThRoW[x] = 255;
			}
			}
		
		} else
		{
	
		    for ( int x = lSx.getXcoord(y); x < lDx.getXcoord(y); x++)
			{
			
			if ( x >= (output.cols/2 - output.cols/16) && x <= (output.cols/2 + output.cols/8) )
			{
			continue;
			}
			
		    if ((HtThRoW[x] >= HthreshLow && HtThRoW[x] <= HthreshHigh && (StThRoW[x] >= 60)) || (VtThRoW[x] >= Vthresh ))
			{
			    outThRoW[x] = 255;
			}
			}
	
		}
	}
	cv::Canny(output,output,10,30);
	
	//ROI per rimozione segnaletica orizzontale
	cv::Rect roi(output.cols/(2.5),0,output.cols/4.5,5*output.rows/5);
	output(roi).setTo(cv::Scalar(0));

	cv::Rect roi1(output.cols/4,2*output.rows/3,output.cols/1.81,output.rows/3);
	output(roi1).setTo(cv::Scalar(0));


	//cv::imshow("lowerProcessedFrame",output);

	//Recupero informazioni (tramite offset) delle coordinate del frame di partenza
	for (int y = 0; y < output.rows; y++)
		{
		unsigned char * outThRoW = output.ptr<unsigned char>(y);
		for ( int x = 0; x < output.cols; x++)
		{
			if ( outThRoW[x] != 0)
			{
		
				if ( x <= output.cols/2)
				{
					selfDCar::xLeftValues.push_back(x+offset.x);
					selfDCar::yLeftValues.push_back(y+offset.y);
				} else
				{
					selfDCar::xRightValues.push_back(x+offset.x);
					selfDCar::yRightValues.push_back(y+offset.y);
				}
			}
		}
		}
}

/**

stima linea sinistra della corsia tramite matrice di Vandermonde

*/
void selfDCar::curveEstimationLeft ()
{
    //Build Vandermonde Matrix
    cv::Mat vandMatrix = cv::Mat::ones( (int) selfDCar::xLeftValues.size(),3,CV_32F);
	cv::Mat yVec = cv::Mat( (int) selfDCar::yLeftValues.size(),1,CV_32F);
	float * ptrVand;
	float * ptrYvec;
    for ( int i = 0; i < selfDCar::xLeftValues.size(); i++ )
	{
	    ptrYvec =  yVec.ptr<float>(i);
		ptrVand =  vandMatrix.ptr<float>(i);
		ptrVand[1] = (float) selfDCar::xLeftValues[i];
		ptrVand[2] = (float) (selfDCar::xLeftValues[i])*(selfDCar::xLeftValues[i]);
		ptrYvec[0] = (float) selfDCar::yLeftValues[i];
		
	}

	try
	{   //calcolo pseudo-inversa
	    cv::Mat parameters = vandMatrix.inv(cv::DECOMP_SVD)*yVec;
		
		//range coefficienti del polinomio in uscita
		if ( parameters.at<float>(1,0) >= -2 && parameters.at<float>(1,0) <= 2)
			{   
			    selfDCar::leftPreviousParam.push_back(parameters);
				if ( selfDCar::leftPreviousParam.size() > selfDCar::preFrame )
				{
			        selfDCar::leftPreviousParam.pop_front();
				}
				if (selfDCar::leftPreviousParam.size() != 0)
				{
					selfDCar::leftCurveParameters = selfDCar::averageMatList(selfDCar::leftPreviousParam);
				}
			}
	} catch (cv::Exception &ex )
	{
		//printf("Error catched:\n");
   
	}

	selfDCar::buildLeftCurve(curveFrame);
}

/**
a partire dai parametri stimati costruisce la linea sinistra della corsia
cv::Mat &originalImg; frame per ricavare le correte coordinate

*/
void selfDCar::buildLeftCurve (cv::Mat & originalImg )
{

	

    int y = 0;
	for (int i = originalImg.cols/8; i < originalImg.cols/2; i ++)
	{
	    y = (int) selfDCar::leftCurveParameters.at<float>(0,0) + selfDCar::leftCurveParameters.at<float>(1,0)*i + selfDCar::leftCurveParameters.at<float>(2,0)*i*i;
	    if ( y >= 11*originalImg.rows/16 && y <= 30*originalImg.rows/32 )
		{
		    selfDCar::leftPoints.push_back(cv::Point(i,y));
		}
	}
}

/**

stima linea destra della corsia tramite matrice di Vandermonde

*/
void selfDCar::curveEstimationRight ()
{
    //Build Vandermonde Matrix
    cv::Mat vandMatrix = cv::Mat::ones( (int) selfDCar::xRightValues.size(),3,CV_32F);
	cv::Mat yVec = cv::Mat( (int) selfDCar::yRightValues.size(),1,CV_32F);
	float * ptrVand;
	float * ptrYvec;
    for ( int i = 0; i < selfDCar::xRightValues.size(); i++ )
	{
	    ptrYvec =  yVec.ptr<float>(i);
		ptrVand =  vandMatrix.ptr<float>(i);
	    ptrVand[1] = (float) selfDCar::xRightValues[i];
		ptrVand[2] = (float) (selfDCar::xRightValues[i])*(selfDCar::xRightValues[i]);
		ptrYvec[0] = (float) selfDCar::yRightValues[i];
		
	}

	try
	{   //calcolo pseudo-inversa
	    cv::Mat parameters = vandMatrix.inv(cv::DECOMP_SVD)*yVec;
		if ( parameters.at<float>(1,0) >= -2 && parameters.at<float>(1,0) <= 2 )
			{   
			    selfDCar::rightPreviousParam.push_back(parameters);
				if ( selfDCar::rightPreviousParam.size() > selfDCar::preFrame )
				{
			        selfDCar::rightPreviousParam.pop_front();
				}
				if (selfDCar::rightPreviousParam.size() != 0)
				{
					selfDCar::rightCurveParameters = selfDCar::averageMatList(selfDCar::rightPreviousParam);
				}
			}
		
	} catch (cv::Exception& e )
	{
		//printf("Error catched:\n");
       
	}

	selfDCar::buildRightCurve(curveFrame);
}


/**

a partire dai parametri stimati costruisce la linea destra della corsia
@cv::Mat &inputRoi; frame di partenza per la corretta stima delle coordinate


*/
void selfDCar::buildRightCurve (cv::Mat & originalImg )
{
    int y = 0;
	
    for (int i = originalImg.cols/2; i < 7*originalImg.cols/8; i ++)
	{
	    y = (int) selfDCar::rightCurveParameters.at<float>(0,0) + selfDCar::rightCurveParameters.at<float>(1,0)*i + selfDCar::rightCurveParameters.at<float>(2,0)*i*i;
	    if ( y >= 11*originalImg.rows/16 && y <= 30*originalImg.rows/32 )
		{   
		    selfDCar::rightPoints.push_back(cv::Point(i,y));
		}
	}

	

}

/**
calcolo media dei parametri dei frame su una finestra temporale fissata
@const std::list<cv::Mat> &list; lista contenente i parametri delle curve degli ultimi "n" frame

*/
cv::Mat selfDCar::averageMatList (const std::list<cv::Mat> & list )
{
    
	std::list<cv::Mat>::const_iterator iterator = list.begin();
	cv::Mat avg = cv::Mat(iterator->rows,iterator->cols,CV_32F,cv::Scalar(0.0));

	for ( size_t i = 0; i < list.size(); i++ )
	{
	    avg += *iterator;
		iterator++;
	}
	
	return (avg/list.size());
}

/**

costruttore della classe Line
@const cv::Point p1; un punto per cui passa retta
@const cv::Point p2; un punto per cui passa retta

*/
utility::Line::Line ( const cv::Point p1,const cv::Point p2 )
{
    // y = mx + q
    /************
	  ||                  ||
     | y1 = mx1 + q      |  m = (y1-y2)/(x1-x2)
	||               ==>|| 
	 | y2 = mx2 + q      |  q = y1 - mx1
	  ||                  ||
	************/
    this->m = (p1.y-p2.y)/float(p1.x-p2.x);
	this->q = p1.y - this->m*p1.x;

}

/**

data la cordinata "y" ritorna la cordinata "x" lungo la retta sulla quale la classe Ŕ stata istanziata
@const int &yCoord; coordinata "y"

*/
int utility::Line::getXcoord (const int & yCoord)
	{
	    return (yCoord-this->q)/this->m;
	}

/**

stima parametri ROI superiore
const cv::Mat *input; frame da elaborare

*/
void selfDCar::upperEstimation (const cv::Mat * input )
{
    selfDCar::xUpLeftValues.clear();
    selfDCar::yUpLeftValues.clear();
	selfDCar::xUpRightValues.clear();
    selfDCar::yUpRightValues.clear();
	selfDCar::upperProcessedFrame = cv::Mat (input[0].rows,input[0].cols,CV_8U,cv::Scalar(0));
	
	//limiti finestra di lavoro
	int rowLine = selfDCar::upperProcessedFrame.rows/9;
	int colLine = selfDCar::upperProcessedFrame.cols/3;
	
	utility::Line lSx = utility::Line ( cv::Point(colLine,0),cv::Point(0,rowLine) );
	utility::Line lDx  =utility::Line ( cv::Point(selfDCar::upperProcessedFrame.cols-colLine,0),cv::Point(selfDCar::upperProcessedFrame.cols,rowLine) );

	for ( int y = 0; y < selfDCar::upperProcessedFrame.rows; y ++ )
	{
	    
	    const unsigned char * HtThRoW = input[0].ptr<unsigned char>(y);
		const unsigned char * StThRoW = input[1].ptr<unsigned char>(y);
		const unsigned char * VtThRoW = input[2].ptr<unsigned char>(y);
		unsigned char * outThRoW = selfDCar::upperProcessedFrame.ptr(y);

		if ( y >= rowLine )
		{
		    
		    for ( int x = 0; x < selfDCar::upperProcessedFrame.cols; x++)
			{
			
			if ( x >= (selfDCar::upperProcessedFrame.cols/2 - selfDCar::upperProcessedFrame.cols/8) && x <= (selfDCar::upperProcessedFrame.cols/2 + selfDCar::upperProcessedFrame.cols/8) )
			{
			    continue;
			}
			
			if ((HtThRoW[x] >= selfDCar::HthreshLow && HtThRoW[x] <= selfDCar::HthreshHigh && (StThRoW[x] >= 30)) || VtThRoW[x] >= selfDCar::Vthresh )
			{
			    outThRoW[x] = 255;
			}
			}
		} else
		{

		    for ( int x = lSx.getXcoord(y); x < lDx.getXcoord(y); x++)
			{
			
			
			if ( x >= (selfDCar::upperProcessedFrame.cols/2 - selfDCar::upperProcessedFrame.cols/16) && x <= (selfDCar::upperProcessedFrame.cols/2 + selfDCar::upperProcessedFrame.cols/8) )
			{
			continue;
			}
			
		    if ((HtThRoW[x] >= selfDCar::HthreshLow && HtThRoW[x] <= selfDCar::HthreshHigh && (StThRoW[x] >= 30)) || (VtThRoW[x] >= selfDCar::Vthresh ))
			{
			    outThRoW[x] = 255;

			}
			}
		}
	}
	
	cv::Canny(selfDCar::upperProcessedFrame,selfDCar::upperProcessedFrame,10,30);


	for (int y = 0; y < selfDCar::upperProcessedFrame.rows; y++)
		{
		unsigned char * outThRoW = selfDCar::upperProcessedFrame.ptr<unsigned char>(y);
		for ( int x = 0; x < selfDCar::upperProcessedFrame.cols; x++)
		{
			if ( outThRoW[x] != 0)
			{
				if ( x <= selfDCar::upperProcessedFrame.cols/2)
				{
					selfDCar::xUpLeftValues.push_back(x+selfDCar::upperOffset.x);
					selfDCar::yUpLeftValues.push_back(y+selfDCar::upperOffset.y);
				} else
				{
					selfDCar::xUpRightValues.push_back(x+selfDCar::upperOffset.x);
					selfDCar::yUpRightValues.push_back(y+selfDCar::upperOffset.y);
				}
			}
		}
		}
	

	std::thread rightUpperCurve(curveEstimationRightUpper);
	std::thread leftUpperCurve(curveEstimationLeftUpper);

	rightUpperCurve.join();
	leftUpperCurve.join();
}

/**

stima linea sinistra (superiore) della corsia tramite matrice di Vandermonde

*/
void selfDCar::curveEstimationLeftUpper ()
{
    //Build Vandermonde Matrix
    cv::Mat vandMatrix = cv::Mat::ones( (int) selfDCar::xUpLeftValues.size(),3,CV_32F);
	cv::Mat yVec = cv::Mat( (int) selfDCar::yUpLeftValues.size(),1,CV_32F);
	float * ptrVand;
	float * ptrYvec;
    for ( int i = 0; i < selfDCar::xUpLeftValues.size(); i++ )
	{
	    ptrYvec =  yVec.ptr<float>(i);
		ptrVand =  vandMatrix.ptr<float>(i);
		ptrVand[1] = (float) selfDCar::xUpLeftValues[i];
		ptrVand[2] = (float) (selfDCar::xUpLeftValues[i])*(selfDCar::xUpLeftValues[i]);
		ptrYvec[0] = (float) selfDCar::yUpLeftValues[i];
		
	}

	try
	{
	    cv::Mat parameters = vandMatrix.inv(cv::DECOMP_SVD)*yVec;
		
		if ( parameters.at<float>(1,0) >= -5 && parameters.at<float>(1,0) <= 5)
			{   
			    selfDCar::leftUpPreviousParam.push_back(parameters);
				if ( selfDCar::leftUpPreviousParam.size() > selfDCar::preFrame )
				{
			        selfDCar::leftUpPreviousParam.pop_front();
				}
				if (selfDCar::leftUpPreviousParam.size() != 0)
				{
					selfDCar::leftUpCurveParameters = selfDCar::averageMatList(selfDCar::leftUpPreviousParam);
				}
			}
		
	} catch (cv::Exception& e )
	{
		//printf("Error catched:\n");
    
	}

	selfDCar::buildUpLeftCurve(selfDCar::curveFrame);
}

/**

costruzione linea sinistra (superiore)
@cv::Mat &originalImage; frame di partenza per recupero corretto di coordinate

*/
void selfDCar::buildUpLeftCurve (cv::Mat & originalImg )
{
    int y = 0;
	

	for (int i = 6*originalImg.cols/16; i < 8*originalImg.cols/16; i ++)
	{
	    y = (int) selfDCar::leftUpCurveParameters.at<float>(0,0) + selfDCar::leftUpCurveParameters.at<float>(1,0)*i + selfDCar::leftUpCurveParameters.at<float>(2,0)*i*i;
	    if ( y >= 10*originalImg.rows/16 && y <= 11*originalImg.rows/16 )
		{
		    selfDCar::leftUpPoints.push_back(cv::Point(i,y));
		}
	}
}

/**

stima linea destra (superiore) della corsia tramite matrice di Vandermonde

*/
void selfDCar::curveEstimationRightUpper ()
{
    //Build Vandermonde Matrix
    cv::Mat vandMatrix = cv::Mat::ones( (int) selfDCar::xUpRightValues.size(),3,CV_32F);
	cv::Mat yVec = cv::Mat( (int) selfDCar::yUpRightValues.size(),1,CV_32F);
	float * ptrVand;
	float * ptrYvec;
    for ( int i = 0; i < selfDCar::xUpRightValues.size(); i++ )
	{
	    ptrYvec =  yVec.ptr<float>(i);
		ptrVand =  vandMatrix.ptr<float>(i);
	    ptrVand[1] = (float) selfDCar::xUpRightValues[i];
		ptrVand[2] = (float) (selfDCar::xUpRightValues[i])*(selfDCar::xUpRightValues[i]);
		ptrYvec[0] = (float) selfDCar::yUpRightValues[i];
		
	}

	try
	{
	    cv::Mat parameters = vandMatrix.inv(cv::DECOMP_SVD)*yVec;
		if ( ( parameters.at<float>(1,0) >= -5 && parameters.at<float>(1,0) <= 5))
			{   
			    selfDCar::rightUpPreviousParam.push_back(parameters);
				if ( selfDCar::rightUpPreviousParam.size() > selfDCar::preFrame )
				{
			        selfDCar::rightUpPreviousParam.pop_front();
				}
				if (selfDCar::rightUpPreviousParam.size() != 0)
				{
					selfDCar::rightUpCurveParameters = selfDCar::averageMatList(selfDCar::rightUpPreviousParam);
				}
			}

	} catch (cv::Exception& e )
	{
		//printf("Error catched:\n");
		
        
	}
	selfDCar::buildUpRightCurve(curveFrame);
}


/**

costruzione linea destra (superiore)
@cv::Mat &originalImage; frame di partenza per recupero corretto di coordinate

*/
void selfDCar::buildUpRightCurve (cv::Mat & originalImg )
{
    int y = 0;
	
    for (int i = 8*originalImg.cols/16; i < 10*originalImg.cols/16; i ++)
	{
	    y = (int) selfDCar::rightUpCurveParameters.at<float>(0,0) + selfDCar::rightUpCurveParameters.at<float>(1,0)*i + selfDCar::rightUpCurveParameters.at<float>(2,0)*i*i;
	    if ( y >= 10*originalImg.rows/16 && y <= 11*originalImg.rows/16 )
		{
		    selfDCar::rightUpPoints.push_back(cv::Point(i,y));
		}
	}

	

}